import { Injectable } from '@angular/core';
import { AirPort, FlightData } from './airport'
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import { MessagesService } from './messages.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FlightsApiService } from './flights-api.service';
import { OnInit } from '@angular/core';


@Injectable()
export class FlightService {

  constructor(private http: HttpClient, private messagesService: MessagesService, private router: Router, private apiService: FlightsApiService) { }

  airportDict: {} = {};
  wizzairAirports = {'TIA': {'name': 'Tirana', 'id': 'TIA', 'country': 'Albania'}, 'VIE': {'name': 'Vienna', 'id': 'VIE', 'country': 'Austria'}, 'GYD': {'name': 'Baku', 'id': 'GYD', 'country': 'Azerbaijan'}, 'CRL': {'name': 'Brussels Charleroi', 'id': 'CRL', 'country': 'Belgium'}, 'SJJ': {'name': 'Sarajevo', 'id': 'SJJ', 'country': 'Bosnia And Herzegovina'}, 'TZL': {'name': 'Tuzla', 'id': 'TZL', 'country': 'Bosnia And Herzegovina'}, 'BOJ': {'name': 'Bourgas (Black Sea)', 'id': 'BOJ', 'country': 'Bulgaria'}, 'SOF': {'name': 'Sofia', 'id': 'SOF', 'country': 'Bulgaria'}, 'VAR': {'name': 'Varna (Black Sea)', 'id': 'VAR', 'country': 'Bulgaria'}, 'OSI': {'name': 'Osijek', 'id': 'OSI', 'country': 'Croatia'}, 'SPU': {'name': 'Split', 'id': 'SPU', 'country': 'Croatia'}, 'LCA': {'name': 'Larnaca', 'id': 'LCA', 'country': 'Cyprus'}, 'PRG': {'name': 'Prague', 'id': 'PRG', 'country': 'Czech Republic'}, 'BLL': {'name': 'Billund', 'id': 'BLL', 'country': 'Denmark'}, 'CPH': {'name': 'Copenhagen', 'id': 'CPH', 'country': 'Denmark'}, 'TLL': {'name': 'Tallinn', 'id': 'TLL', 'country': 'Estonia'}, 'TKU': {'name': 'Turku', 'id': 'TKU', 'country': 'Finland'}, 'MLH': {'name': 'Basel-Mulhouse-Freiburg', 'id': 'MLH', 'country': 'France'}, 'BOD': {'name': 'Bordeaux (Mérignac Airport)', 'id': 'BOD', 'country': 'France'}, 'GNB': {'name': 'Grenoble', 'id': 'GNB', 'country': 'France'}, 'LYS': {'name': 'Lyon -Saint-Exupéry', 'id': 'LYS', 'country': 'France'}, 'NCE': {'name': 'Nice', 'id': 'NCE', 'country': 'France'}, 'BVA': {'name': 'Paris Beauvais', 'id': 'BVA', 'country': 'France'}, 'KUT': {'name': 'Kutaisi', 'id': 'KUT', 'country': 'Georgia'}, 'SXF': {'name': 'Berlin Schoenefeld', 'id': 'SXF', 'country': 'Germany'}, 'CGN': {'name': 'Cologne', 'id': 'CGN', 'country': 'Germany'}, 'DTM': {'name': 'Dortmund', 'id': 'DTM', 'country': 'Germany'}, 'FRA': {'name': 'Frankfurt', 'id': 'FRA', 'country': 'Germany'}, 'HHN': {'name': 'Frankfurt Hahn', 'id': 'HHN', 'country': 'Germany'}, 'FDH': {'name': 'Friedrichshafen', 'id': 'FDH', 'country': 'Germany'}, 'HAM': {'name': 'Hamburg', 'id': 'HAM', 'country': 'Germany'}, 'HAJ': {'name': 'Hanover', 'id': 'HAJ', 'country': 'Germany'}, 'FKB': {'name': 'Karlsruhe/Baden-Baden', 'id': 'FKB', 'country': 'Germany'}, 'FMM': {'name': 'Memmingen/Munich West', 'id': 'FMM', 'country': 'Germany'}, 'NUE': {'name': 'Nuremberg', 'id': 'NUE', 'country': 'Germany'}, 'ens': {'name': 'At', 'id': 'ens', 'country': 'Greece'}, 'CFU': {'name': 'Corfu', 'id': 'CFU', 'country': 'Greece'}, 'HER': {'name': 'Heraklion (Crete)', 'id': 'HER', 'country': 'Greece'}, 'KVA': {'name': 'Kavala International Airport', 'id': 'KVA', 'country': 'Greece'}, 'RHO': {'name': 'Rhodes', 'id': 'RHO', 'country': 'Greece'}, 'SKG': {'name': 'Thessaloniki', 'id': 'SKG', 'country': 'Greece'}, 'ZTH': {'name': 'Zakynthos', 'id': 'ZTH', 'country': 'Greece'}, 'BUD': {'name': 'Budapest', 'id': 'BUD', 'country': 'Hungary'}, 'DEB': {'name': 'Debrecen', 'id': 'DEB', 'country': 'Hungary'}, 'KEF': {'name': 'Reykjavik', 'id': 'KEF', 'country': 'Iceland'}, 'VDA': {'name': 'Eilat (Ovda)', 'id': 'VDA', 'country': 'Israel'}, 'TLV': {'name': 'Tel-Aviv', 'id': 'TLV', 'country': 'Israel'}, 'AHO': {'name': 'Alghero (Sardinia)', 'id': 'AHO', 'country': 'Italy'}, 'BRI': {'name': 'Bari', 'id': 'BRI', 'country': 'Italy'}, 'BLQ': {'name': 'Bologna', 'id': 'BLQ', 'country': 'Italy'}, 'CTA': {'name': 'Catania (Sicily)', 'id': 'CTA', 'country': 'Italy'}, 'SUF': {'name': 'Lamezia Terme', 'id': 'SUF', 'country': 'Italy'}, 'BGY': {'name': 'Milan Bergamo', 'id': 'BGY', 'country': 'Italy'}, 'MXP': {'name': 'Milan Malpensa', 'id': 'MXP', 'country': 'Italy'}, 'NAP': {'name': 'Naples', 'id': 'NAP', 'country': 'Italy'}, 'PEG': {'name': 'Perugia', 'id': 'PEG', 'country': 'Italy'}, 'PSR': {'name': 'Pescara', 'id': 'PSR', 'country': 'Italy'}, 'PSA': {'name': 'Pisa (Tuscany)', 'id': 'PSA', 'country': 'Italy'}, 'CIA': {'name': 'Rome Ciampino', 'id': 'CIA', 'country': 'Italy'}, 'FCO': {'name': 'Rome Fiumicino', 'id': 'FCO', 'country': 'Italy'}, 'TRN': {'name': 'Turin', 'id': 'TRN', 'country': 'Italy'}, 'TSF': {'name': 'Venice Treviso', 'id': 'TSF', 'country': 'Italy'}, 'VRN': {'name': 'Verona', 'id': 'VRN', 'country': 'Italy'}, 'TSE': {'name': 'Astana', 'id': 'TSE', 'country': 'Kazakhstan'}, 'PRN': {'name': 'Prishtina', 'id': 'PRN', 'country': 'Kosovo'}, 'RIX': {'name': 'Riga', 'id': 'RIX', 'country': 'Latvia'}, 'KUN': {'name': 'Kaunas', 'id': 'KUN', 'country': 'Lithuania'}, 'PLQ': {'name': 'Palanga – Klaipeda', 'id': 'PLQ', 'country': 'Lithuania'}, 'VNO': {'name': 'Vilnius', 'id': 'VNO', 'country': 'Lithuania'}, 'OHD': {'name': 'Ohrid', 'id': 'OHD', 'country': 'Macedonia'}, 'SKP': {'name': 'Skopje', 'id': 'SKP', 'country': 'Macedonia'}, 'MLA': {'name': 'Malta', 'id': 'MLA', 'country': 'Malta'}, 'KIV': {'name': 'Chisinau', 'id': 'KIV', 'country': 'Moldova'}, 'TGD': {'name': 'Podgorica', 'id': 'TGD', 'country': 'Montenegro'}, 'AGA': {'name': 'Agadir', 'id': 'AGA', 'country': 'Morocco'}, 'EIN': {'name': 'Eindhoven', 'id': 'EIN', 'country': 'Netherlands'}, 'GRQ': {'name': 'Groningen', 'id': 'GRQ', 'country': 'Netherlands'}, 'AES': {'name': 'Alesund', 'id': 'AES', 'country': 'Norway'}, 'BGO': {'name': 'Bergen', 'id': 'BGO', 'country': 'Norway'}, 'HAU': {'name': 'Haugesund', 'id': 'HAU', 'country': 'Norway'}, 'KRS': {'name': 'Kristiansand', 'id': 'KRS', 'country': 'Norway'}, 'MOL': {'name': 'Molde', 'id': 'MOL', 'country': 'Norway'}, 'TRF': {'name': 'Oslo Sandefjord Torp', 'id': 'TRF', 'country': 'Norway'}, 'SVG': {'name': 'Stavanger', 'id': 'SVG', 'country': 'Norway'}, 'TRD': {'name': 'Trondheim', 'id': 'TRD', 'country': 'Norway'}, 'TOS': {'name': 'Tromso', 'id': 'TOS', 'country': 'Norway'}, 'GDN': {'name': 'Gdansk', 'id': 'GDN', 'country': 'Poland'}, 'KTW': {'name': 'Katowice', 'id': 'KTW', 'country': 'Poland'}, 'LUZ': {'name': 'Lublin', 'id': 'LUZ', 'country': 'Poland'}, 'SZY': {'name': 'Olsztyn-Mazury', 'id': 'SZY', 'country': 'Poland'}, 'POZ': {'name': 'Poznan', 'id': 'POZ', 'country': 'Poland'}, 'SZZ': {'name': 'Szczecin', 'id': 'SZZ', 'country': 'Poland'}, 'WAW': {'name': 'Warsaw Chopin', 'id': 'WAW', 'country': 'Poland'}, 'WRO': {'name': 'Wroclaw', 'id': 'WRO', 'country': 'Poland'}, 'FAO': {'name': 'Faro', 'id': 'FAO', 'country': 'Portugal'}, 'LIS': {'name': 'Lisbon', 'id': 'LIS', 'country': 'Portugal'}, 'OPO': {'name': 'Porto', 'id': 'OPO', 'country': 'Portugal'}, 'OTP': {'name': 'Bucharest', 'id': 'OTP', 'country': 'Romania'}, 'CLJ': {'name': 'Cluj-Napoca', 'id': 'CLJ', 'country': 'Romania'}, 'CND': {'name': 'Constanta', 'id': 'CND', 'country': 'Romania'}, 'CRA': {'name': 'Craiova', 'id': 'CRA', 'country': 'Romania'}, 'IAS': {'name': 'Iasi', 'id': 'IAS', 'country': 'Romania'}, 'SUJ': {'name': 'Satu Mare', 'id': 'SUJ', 'country': 'Romania'}, 'SBZ': {'name': 'Sibiu', 'id': 'SBZ', 'country': 'Romania'}, 'SCV': {'name': 'Suceava', 'id': 'SCV', 'country': 'Romania'}, 'TSR': {'name': 'Timisoara', 'id': 'TSR', 'country': 'Romania'}, 'TGM': {'name': 'Tirgu Mures', 'id': 'TGM', 'country': 'Romania'}, 'VKO': {'name': 'Moscow', 'id': 'VKO', 'country': 'Russia'}, 'LED': {'name': 'St Petersburg', 'id': 'LED', 'country': 'Russia'}, 'BEG': {'name': 'Belgrade', 'id': 'BEG', 'country': 'Serbia'}, 'INI': {'name': 'Niš', 'id': 'INI', 'country': 'Serbia'}, 'BTS': {'name': 'Bratislava', 'id': 'BTS', 'country': 'Slovakia'}, 'KSC': {'name': 'Kosice', 'id': 'KSC', 'country': 'Slovakia'}, 'TAT': {'name': 'Poprad-Tatry', 'id': 'TAT', 'country': 'Slovakia'}, 'LJU': {'name': 'Ljubljana', 'id': 'LJU', 'country': 'Slovenia'}, 'ALC': {'name': 'Alicante', 'id': 'ALC', 'country': 'Spain'}, 'BCN': {'name': 'Barcelona El Prat', 'id': 'BCN', 'country': 'Spain'}, 'FUE': {'name': 'Fuerteventura (Canary Islands)', 'id': 'FUE', 'country': 'Spain'}, 'IBZ': {'name': 'Ibiza', 'id': 'IBZ', 'country': 'Spain'}, 'ACE': {'name': 'Lanzarote (Canary Islands)', 'id': 'ACE', 'country': 'Spain'}, 'MAD': {'name': 'Madrid', 'id': 'MAD', 'country': 'Spain'}, 'AGP': {'name': 'Malaga', 'id': 'AGP', 'country': 'Spain'}, 'PMI': {'name': 'Palma de Mallorca', 'id': 'PMI', 'country': 'Spain'}, 'SDR': {'name': 'Santander', 'id': 'SDR', 'country': 'Spain'}, 'TFS': {'name': 'Tenerife (Canary Islands)', 'id': 'TFS', 'country': 'Spain'}, 'VLC': {'name': 'Valencia', 'id': 'VLC', 'country': 'Spain'}, 'ZAZ': {'name': 'Zaragoza', 'id': 'ZAZ', 'country': 'Spain'}, 'GOT': {'name': 'Gothenburg Landvetter', 'id': 'GOT', 'country': 'Sweden'}, 'MMX': {'name': 'Malmo', 'id': 'MMX', 'country': 'Sweden'}, 'NYO': {'name': 'Stockholm Skavsta', 'id': 'NYO', 'country': 'Sweden'}, 'VXO': {'name': 'Vaxjo', 'id': 'VXO', 'country': 'Sweden'}, 'BSL': {'name': 'Basel-Mulhouse-Freiburg', 'id': 'BSL', 'country': 'Switzerland'}, 'GVA': {'name': 'Geneva', 'id': 'GVA', 'country': 'Switzerland'}, 'HRK': {'name': 'Kharkiv', 'id': 'HRK', 'country': 'Ukraine'}, 'IEV': {'name': 'Kiev - Zhulyany', 'id': 'IEV', 'country': 'Ukraine'}, 'LWO': {'name': 'Lviv', 'id': 'LWO', 'country': 'Ukraine'}, 'DWC': {'name': 'Dubai', 'id': 'DWC', 'country': 'United Arab Emirates'}, 'ABZ': {'name': 'Aberdeen', 'id': 'ABZ', 'country': 'United Kingdom'}, 'BFS': {'name': 'Belfast', 'id': 'BFS', 'country': 'United Kingdom'}, 'BHX': {'name': 'Birmingham', 'id': 'BHX', 'country': 'United Kingdom'}, 'BRS': {'name': 'Bristol', 'id': 'BRS', 'country': 'United Kingdom'}, 'DSA': {'name': 'Doncaster/Sheffield', 'id': 'DSA', 'country': 'United Kingdom'}, 'GLA': {'name': 'Glasgow', 'id': 'GLA', 'country': 'United Kingdom'}, 'LPL': {'name': 'Liverpool', 'id': 'LPL', 'country': 'United Kingdom'}, 'LTN': {'name': 'London Luton', 'id': 'LTN', 'country': 'United Kingdom'}, 'LGW': {'name': 'London Gatwick', 'id': 'LGW', 'country': 'United Kingdom'}};
  airportList = new BehaviorSubject<AirPort[]>([]);
  airportFiltersChanged = new BehaviorSubject<AirPort[]>([]);
  airportListTo = new BehaviorSubject<AirPort[]>([]);
  airportListFrom = new BehaviorSubject<AirPort[]>([]);
  addedAirportsIdsFrom: string[] = [];
  addedAirportsIdsTo: string[] = [];
  date_from: Date;
  date_to: Date;
  results = new Subject<FlightData[]>();
  resetResults = new Subject();
  flexDays = 6;

  private log(message: string) {
    this.messagesService.add('FlightService: ' + message);
  }

  initService() {
    this.apiService.getAirports().subscribe(airports => {
      this.airportDict = airports;
      for(const airport of Object.keys(this.wizzairAirports)){
        if(!this.airportDict[airport]){
          this.airportDict[airport] = this.wizzairAirports[airport];
        }
      }
      this.airportList.next(this.getAirportList(Object.keys(airports)));
    })
  }

  getAirportsOptions(target): Observable<AirPort[]> {
    return this.airportList
  }

  getAirportList(keys): AirPort[]{
    return keys.map(x => this.airportDict[x]).sort((a1, a2) => { return a1.name > a2.name ? 1 : -1})
  }

  getAirportListSubscription(target: string): BehaviorSubject<AirPort[]> {
    return target === 'from' ? this.airportListFrom : this.airportListTo;
  }

  addAirportToList(airport: AirPort, list_type: string) {
    let airports: string[] = list_type === 'from' ? this.addedAirportsIdsFrom : this.addedAirportsIdsTo;
    airports.indexOf(airport.id) == -1 && airports.push(airport.id) && this.updateAddedList(list_type);
  }

  addPoland(list_type: string): void {
    let airports = this.airportList.getValue();
    for(const airport of Object.keys(airports)){
      if(airports[airport].country === 'Poland'){
        this.addAirportToList(airports[airport], list_type)
      }
    }
  }

  deleteFromList(airport: AirPort, target: string){
    let airports: string[] = target === 'from' ? this.addedAirportsIdsFrom : this.addedAirportsIdsTo;
    airports.indexOf(airport.id) != -1 && airports.splice(airports.indexOf(airport.id), 1) && this.updateAddedList(target);
  }

  updateAddedList(target){
    if(target === 'from'){this.airportListFrom.next(this.getAirportList(this.addedAirportsIdsFrom));}
    else {this.airportListTo.next(this.getAirportList(this.addedAirportsIdsTo));}
  }

  search(){
    this.resetResults.next(true);
    this.getResults();
  }

  getResults(): void{
    let returnDate = this.formatDate(this.date_to);
    let startDate = this.formatDate(this.date_from);
    for(let origin of this.addedAirportsIdsFrom){
      for(let dest of this.addedAirportsIdsTo){
        this.apiService.getFlightAvailabilityRyanAir(this.airportDict[origin], this.airportDict[dest], false, startDate, this.flexDays).subscribe(flights => this.results.next(flights));
        this.apiService.getFlightAvailabilityRyanAir(this.airportDict[dest], this.airportDict[origin], true, returnDate, this.flexDays).subscribe(flights => this.results.next(flights));
        this.apiService.getFlightAvailabilityWizzAir(this.airportDict[origin], this.airportDict[dest], false, startDate, this.flexDays).subscribe(flights => this.results.next(flights));
        this.apiService.getFlightAvailabilityWizzAir(this.airportDict[dest], this.airportDict[origin], true, returnDate, this.flexDays).subscribe(flights => this.results.next(flights));
        // this.apiService.getFlightAvailabilityEasyJet(this.airportDict[origin], this.airportDict[dest], false, startDate, this.flexDays).subscribe(flights => this.results.next(flights));
        // this.apiService.getFlightAvailabilityEasyJet(this.airportDict[dest], this.airportDict[origin], true, returnDate, this.flexDays).subscribe(flights => this.results.next(flights));
      }
    }
  }

  private formatDate(date: Date): string{
    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()
    return `${year}-${month}-${day}`;
  }

}
