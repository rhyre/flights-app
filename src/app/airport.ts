export class AirPort {

  constructor(id: string = '', name: string = '', country: string = ''){
      this.id = id;
      this.name = name;
      this.country = country;
  }

  id: string;
  name: string;
  country: string;
}

export class FlightData {

  constructor(from: AirPort, to: AirPort, date: Date, price: string, isReturn: boolean, currency: string, carrier: string) {
    this.from = from;
    this.to = to;
    this.date = date;
    this.price = price;
    this.isReturn = isReturn;
    this.currency = currency;
    this.carrier = carrier;
  }

  from: AirPort;
  to: AirPort;
  date: Date;
  price: string;
  isReturn: boolean;
  currency: string;
  carrier: string;
}
