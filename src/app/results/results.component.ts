import { Component, OnInit, Input } from '@angular/core';
import { AirPort, FlightData } from '../airport';
import { FlightService } from '../flight.service';
import { MessagesService } from '../messages.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  constructor(private flightService: FlightService, private messagesService: MessagesService) { }

  results: FlightData[] = [];
  resultsReturn: FlightData[] = [];
  min_value_dest: number = 0;
  min_value_ret: number = 0;

  ngOnInit() {
      this.flightService.resetResults.subscribe(x => {this.results = []; this.resultsReturn = []})
      this.flightService.results.subscribe(results => {
          for(let result of results){
              if(result.isReturn) {
                  this.resultsReturn.push(result)}
              else {
                  this.results.push(result)}}
          this.updateColors();
      });
  }

  updateColors(){
      this.min_value_dest = 9999999;
      this.min_value_ret = 9999999;
      for (let res of this.results) {
          this.min_value_dest = this.min_value_dest >= parseInt(res.price) ? parseInt(res.price) : this.min_value_dest;}
      for (let res of this.resultsReturn) {
          this.min_value_ret = this.min_value_ret >= parseInt(res.price) ? parseInt(res.price) : this.min_value_ret;}
      this.min_value_dest = this.min_value_dest + 0.1 * this.min_value_dest;
      this.min_value_ret = this.min_value_ret + 0.1 * this.min_value_ret;

  }

  sortPrice(){
      this.results = this.results.sort((a, b) => parseInt(a.price) - parseInt(b.price));
      this.resultsReturn = this.resultsReturn.sort((a, b) => parseInt(a.price) - parseInt(b.price));
  }

  sortDate(){
      this.results = this.results.sort((a, b) => a.date > b.date ? 1 : -1);
      this.resultsReturn = this.resultsReturn.sort((a, b) => a.date > b.date ? 1 : -1);
  }
}
