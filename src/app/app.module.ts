import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {SuiModule} from 'ng2-semantic-ui';

import { AppComponent } from './app.component';
import { SelectPanelComponent } from './dashboard/select-panel/select-panel.component';
import { SelectComponent } from './dashboard/select-panel/select/select.component';
import { ListComponent } from './dashboard/select-panel/list/list.component';
import { DetailsComponent } from './dashboard/select-panel/details/details.component';
import { AppRoutingModule } from './/app-routing.module';
import { MessagesComponent } from './messages/messages.component';
import { MessagesService } from './messages.service';
import { FlightService } from './flight.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DatefieldComponent } from './dashboard/datefield/datefield.component';
import { ResultsComponent } from './results/results.component';
import { FlightsApiService } from './flights-api.service';


@NgModule({
  declarations: [
    AppComponent,
    SelectPanelComponent,
    SelectComponent,
    ListComponent,
    DetailsComponent,
    MessagesComponent,
    DashboardComponent,
    DatefieldComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SuiModule
  ],
  providers: [
    MessagesService,
    FlightService,
    FlightsApiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
