import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessagesService } from './messages.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { AirPort, FlightData } from './airport'


@Injectable()
export class FlightsApiService {
  wizziarAPIVersion = '8.2.0';

  constructor(private http: HttpClient,  private messagesService: MessagesService) {
    this.setWizzAirAPI().subscribe(api => {
      this.wizziarAPIVersion = api.substring(api.indexOf('.com/') + 5, api.indexOf('/Api'))
    })
  }
  private log(message: string) {
    this.messagesService.add('FlightsApiService: ' + message);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  setWizzAirAPI(): Observable<string>{
     let wizzAirApi = `/static/metadata.json`
     return this.http.get<{}>(wizzAirApi).pipe(
      map( (response) => {
        return response['apiUrl'];}))}

  getAirports(): Observable<{}>{
    let ryanairApi: string = '/aggregate/4/common?embedded=airports,countries';
    return this.http.get<AirPort[]>(ryanairApi).pipe(
        map( (response: AirPort[]) => {
          let airportDict: {} = {}
          let countries = {}
          for (const country of response['countries']){
            countries[country['code']] = country['name']}
          for ( const airport of response['airports']){
            airportDict[airport['iataCode']] = new AirPort(airport['iataCode'], airport['name'], countries[airport['countryCode']]);}
          return airportDict;}),
        catchError(this.handleError('getAirports', {})));}

  getFlightAvailabilityRyanAir(originAirport: AirPort, destinationAirport: AirPort, isReturn: boolean, startDate: string, flexDays: number): Observable<FlightData[]> {
    let dest = destinationAirport.id;
    let origin = originAirport.id;
    let ryanAirApi = `/v4/pl-pl/availability?DateOut=${startDate}&Destination=${dest}&FlexDaysOut=${flexDays}&Origin=${origin}&ToUs=AGREED`
    return this.http.get<{}>(ryanAirApi).pipe(
      map( (response) => {
        let currency = response['currency'];
        let dates = response['trips'][0]['dates'].filter(dates => dates['flights'].length !== 0);
        let flightData = [];
        for(let date of dates){
          for(let flight of date['flights']){
            if(flight['faresLeft'] !== 0){
            flightData.push(new FlightData(originAirport, destinationAirport, flight['time'][0], flight['regularFare']['fares'][0]['amount'], isReturn, currency, 'FR'));}}}
        return flightData;}))}

  getFlightAvailabilityWizzAir(originAirport: AirPort, destinationAirport: AirPort, isReturn: boolean, startDate: string, flexDays: number): Observable<FlightData[]> {
    let dest = destinationAirport.id;
    let origin = originAirport.id;
    let wizzAirApi = `/${this.wizziarAPIVersion}/Api/asset/farechart`;
    let data = {"wdc":true,"flightList":[{"departureStation":origin,"arrivalStation":dest,"date":startDate}],"dayInterval":3,"adultCount":1,"childCount":0,"isRescueFare":false};
    return this.http.post<{}>(wizzAirApi, data).pipe(
      map( (response) => {
        let flights = response['outboundFlights'];
        let flightData = [];
        for(let flight of flights){
            if(flight['price'] !== null){
              flightData.push(new FlightData(originAirport, destinationAirport, flight['date'], flight['price']['amount'], isReturn, flight['price']['currencyCode'], 'W6'));}}
        return flightData;}))}

  getFlightAvailabilityEasyJet(originAirport: AirPort, destinationAirport: AirPort, isReturn: boolean, startDate: string, flexDays: number): Observable<FlightData[]> {
    let dest = destinationAirport.id;
    let origin = originAirport.id;
    let date = new Date(startDate);
    let prevDay = new Date(startDate)
    prevDay.setDate(date.getDate()-1)
    let prevDayFormatted = this.formatDate(prevDay)
    let nextDay = new Date(startDate)
    nextDay.setDate(date.getDate()+1)
    let nextDayFormatted = this.formatDate(nextDay)
    let easyJetApi = `/ejavailability/api/v19/availability/query?AdultSeats=1&ArrivalIata=${dest}&DepartureIata=${origin}&IncludePrices=true&LanguageCode=PL&MaxDepartureDate=${nextDayFormatted}&MinDepartureDate=${prevDayFormatted}`;
    return this.http.get<{}>(easyJetApi).pipe(
      map( (response) => {
        let flights = response['AvailableFlights'];
        let currency = response['DepartureBaseCurrencyCode']
        let flightData = [];
        for(let flight of flights){
            this.log(flight);
            if(flight['FlightFares']['SeatsAvailable'] !== 0){
              this.log('dodaje');
              flightData.push(new FlightData(originAirport, destinationAirport, flight['LocalArrivalTime'], flight['FlightFares']['Prices']['Adult']['PriceWithCreditCard'], isReturn, currency, 'EZY'));}}
        return flightData;}))}

  private formatDate(date: Date): string{
    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()
    return `${year}-${month}-${day}`;
  }
 }
