import { Component, OnInit, Input } from '@angular/core';
import { AirPort } from '../../airport';
import { FlightService } from '../../flight.service';

@Component({
  selector: 'app-select-panel',
  templateUrl: './select-panel.component.html',
  styleUrls: ['./select-panel.component.css']
})
export class SelectPanelComponent implements OnInit {

  @Input() target: string;
  airportSelected: AirPort = new AirPort();

  constructor(public flightService: FlightService) { }

  ngOnInit() {
  }

  onSelectedAirport(airport: AirPort){
    this.airportSelected = airport;
  }
}
