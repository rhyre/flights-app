import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { AirPort } from '../../../airport';
import { FlightService } from '../../../flight.service';
import { Subscription } from 'rxjs/Subscription'

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit, OnDestroy {

  constructor(private flightService: FlightService) { }

  @Output() selectedAirport = new EventEmitter<AirPort>();
  @Input() target: string;
  options: AirPort[];
  selectedOption: AirPort;
  private subscription: Subscription;

  getAirports(): void {
    this.subscription = this.flightService.getAirportsOptions(this.target).subscribe(airports => this.options = airports);
  }

  onSelectedAirport(airport: AirPort){
    this.selectedAirport.emit(airport);
    this.flightService.addAirportToList(airport, this.target);
  }

  public formatter(option:AirPort, query?:string):string {
      let option_formated = `${option.name} (${option.id})`;
      return option_formated.replace(query,`<b>${query}</b>`);
  }

  public filter(options: AirPort[], query?:string): AirPort[]{
    return options.filter(option => {return option.name.toLowerCase().includes(query.toLowerCase()) || option.id.toLowerCase().includes(query.toLowerCase())})
  }

  addPoland(): void {
    this.flightService.addPoland(this.target)
  }

  ngOnInit() {
    this.getAirports();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
