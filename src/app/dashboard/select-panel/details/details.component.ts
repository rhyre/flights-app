import { Component, OnInit, Input } from '@angular/core';
import { AirPort } from '../../../airport';
import { FlightService } from '../../../flight.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private flightService: FlightService) { }

  @Input() airport: AirPort;
  @Input() target: string;

  ngOnInit() {
  }

  addAirportToList(airport: AirPort) {
      this.flightService.addAirportToList(airport, this.target);
  }

}
