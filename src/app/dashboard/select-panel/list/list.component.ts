import { Component, OnInit, Input } from '@angular/core';
import { AirPort } from '../../../airport';
import { FlightService } from '../../../flight.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  airports: AirPort[] = [];
  @Input() target: string;

  constructor(private flightService: FlightService) { }

  ngOnInit() {
    this.flightService.getAirportListSubscription(this.target).subscribe(airports => this.airports = airports);
  }

  deleteFromList(airport: AirPort) {
    this.flightService.deleteFromList(airport, this.target);
  }

}
