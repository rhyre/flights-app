import { Component, OnInit, Input } from '@angular/core';
import { FlightService } from '../../flight.service';

@Component({
  selector: 'app-datefield',
  templateUrl: './datefield.component.html',
  styleUrls: ['./datefield.component.css']
})
export class DatefieldComponent implements OnInit {

  constructor(private flightService: FlightService) { }

  @Input() target: string;

  date_field: Date;

  ngOnInit() {
  }

  onSelectedDate(){
    if(this.target === 'from')
      this.flightService.date_from = this.date_field;
    else
      this.flightService.date_to = this.date_field;
  }

}
