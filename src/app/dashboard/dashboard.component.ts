import { Component, OnInit } from '@angular/core';
import { FlightService } from '../flight.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  target_to = "to";
  target_from = "from";
  constructor(public flightService: FlightService) { }

  ngOnInit() {
  }

  onSearchClicked(){
      this.flightService.search();
  }

}
