var HttpsProxyAgent = require('https-proxy-agent');
var proxyConfig = [

  {
    context: ['/aggregate/', '/timetable/'],
    target: 'https://api.ryanair.com',
    secure: false,
    changeOrigin: true,
    logLevel: "debug",
  },
  {
    context: '/v4/',
    target: 'https://desktopapps.ryanair.com',
    secure: false,
    changeOrigin: true,
    logLevel: "debug",
  },
  {
    context: '/8.',
    target: 'https://be.wizzair.com',
    secure: false,
    changeOrigin: true,
    logLevel: "debug",
  },
  {
    context: '/9.',
    target: 'https://be.wizzair.com',
    secure: false,
    changeOrigin: true,
    logLevel: "debug",
  },
  {
    context: '/static/metadata.json',
    target: 'https://wizzair.com',
    secure: false,
    changeOrigin: true,
    logLevel: "debug",
  },
  {
    context: '/ejavailability/',
    target: 'https://easyjet.com',
    secure: false,
    changeOrigin: true,
    logLevel: "debug",
  }

];



function setupForCorporateProxy(proxyConfig) {
  var proxyServer = process.env.http_proxy || process.env.HTTP_PROXY;
  if (proxyServer) {
    var agent = new HttpsProxyAgent(proxyServer);
    proxyConfig.forEach(function(entry) {
      entry.agent = agent;
    });
  }
  return proxyConfig;
}

module.exports = setupForCorporateProxy(proxyConfig);
